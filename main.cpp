#include <iostream>
#include <vector>
#include <random>
#include "func.h"
#include "func_t.h"

int main()
{
    std::cout << std::boolalpha;
    std::cout << "fuzzy_search(\"car\", \"cartwheel\") is ";
    std::cout << fuzzy_search("car", "cartwheel") << std::endl;       // true
    std::cout << "fuzzy_search(\"cwhl\", \"cartwheel\") is ";
    std::cout << fuzzy_search("cwhl", "cartwheel") << std::endl;      // true
    std::cout << "fuzzy_search(\"cwheel\", \"cartwheel\") is ";
    std::cout << fuzzy_search("cwheel", "cartwheel") << std::endl;    // true
    std::cout << "fuzzy_search(\"cartwheel\", \"cartwheel\") is ";
    std::cout << fuzzy_search("cartwheel", "cartwheel") << std::endl; // true
    std::cout << "fuzzy_search(\"cwheeel\", \"cartwheel\") is ";
    std::cout << fuzzy_search("cwheeel", "cartwheel") << std::endl;   // false
    std::cout << "fuzzy_search(\"lw\", \"cartwheel\") is ";
    std::cout << fuzzy_search("lw", "cartwheel") << std::endl;        // false
    std::cout << "fuzzy_search(\"l\", \"cartwheel\") is ";
    std::cout << fuzzy_search("l", "cartwheel") << std::endl;         // true
    std::cout << std::endl;
//-----------------------------------------------------------------------------
    std::cout << "gcd(544, 119) = " << gcd(544, 119) << std::endl;
    std::cout << "factorial(6) = " << factorial(6) << std::endl;
    std::cout << "factorial(17) = " << factorial(17) << std::endl;
    std::cout << std::endl;
//-----------------------------------------------------------------------------
    std::vector<int> array;
    int elem = 0;
    LLONG index = -1;
    size_t lenght = 0x10;
    init_vec(array, lenght);
    std::cout << "source vector:\n";
    print_vec(array);

    revers_vec(array);
    std::cout << "after reverse:\n";
    print_vec(array);

    quick_sort(array, 0, array.size()-1);
    std::cout << "after quick_sort:\n";
    print_vec(array);

    merge_sort(array, 0, array.size()-1);
    std::cout << "after merge_sort:\n";
    print_vec(array);

    insert_sort(array);
    std::cout << "after insert_sort:\n";
    print_vec(array);

    select_sort(array);
    std::cout << "after select_sort:\n";
    print_vec(array);

    std::cout << "enter the number for search in vector:\n";
    std::cin >> elem;

    index = bin_search(array, elem);
    std::cout << "bin search result:\n";
    if(index < 0)
        std::cout << "NOT FOUND!\n";
    else
        std::cout << "index = " << index << std::endl;

    index = lin_search(array, elem);
    std::cout << "linear search result:\n";
    if(index < 0)
        std::cout << "NOT FOUND!\n";
    else
        std::cout << "index = " << index << std::endl;

    return 0;
}
