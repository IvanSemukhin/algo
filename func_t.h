#ifndef FUNC_T_H
#define FUNC_T_H

typedef signed long long LLONG;
constexpr double PI = 3.1415;
//-----------------------------------------------------------------------------

template <class X> void init_vec(std::vector<X>& vec, size_t kol)
{
    vec.clear();
    std::mt19937 gen;
    std::uniform_int_distribution<> uid(0, 30);
    for(size_t i = 0; i < kol; i++)
        vec.push_back(uid(gen) / PI);
}

template <class X> void print_vec(const std::vector<X>& vec)
{
    for(auto& elem : vec)
        std::cout << elem << ' ';
    std::cout << std::endl;
}

template <class X> void revers_vec(std::vector<X>& vec)
{
    size_t left = 0;
    size_t right = vec.size()-1;
    while(left < right)
        std::swap(vec.at(left++), vec.at(right--));
}
//-----------------------------------------------------------------------------

template <class IntType> IntType gcd(IntType a, IntType b)
{
    return b ? gcd(b, a%b) : a;
}
//-----------------------------------------------------------------------------

template <class X> void quick_sort(std::vector<X>& vec, LLONG left, LLONG right)
{
    if(left < right){
        LLONG sup = left;
        for(LLONG i = left; i < right; i++)
            if(vec.at(i) < vec.at(right))
                std::swap(vec.at(sup++), vec.at(i));
        std::swap(vec.at(sup), vec.at(right));
        quick_sort(vec, left, sup-1);
        quick_sort(vec, sup+1, right);
    }
}

template <class X> void merge_sort(std::vector<X>& vec, LLONG left, LLONG right)
{
    if(left < right){
        LLONG half = (left + right)/2;
        merge_sort(vec, left, half);
        merge_sort(vec, half+1, right);
        LLONG pos1 = left;
        LLONG pos2 = half+1;
        LLONG pos3 = 0;
        LLONG size = right-left+1;
        std::vector<X> tmp(size);
        while(pos1 <= half && pos2 <= right)
            if(vec.at(pos1) < vec.at(pos2))
                tmp.at(pos3++) = vec.at(pos1++);
            else
                tmp.at(pos3++) = vec.at(pos2++);
        while(pos1 <= half)
            tmp.at(pos3++) = vec.at(pos1++);
        while(pos2 <= right)
            tmp.at(pos3++) = vec.at(pos2++);
        for(pos3 = 0; pos3 < size; pos3++)
            vec.at(left+pos3) = tmp.at(pos3);
        tmp.clear();
    }
}

template <class X> void insert_sort(std::vector<X>& vec)
{
    size_t len = vec.size();
    for(size_t i = 1; i< len; i++){
        X elem = vec.at(i);
        size_t j = i-1;
        while(j >=0 && vec.at(j) > elem){
            vec.at(j+1) = vec.at(j);
            j--;
        }
        vec.at(j+1) = elem;
    }
}

template <class X> void select_sort(std::vector<X>& vec)
{
    size_t len = vec.size();
    for(size_t i = 0; i < len-1; i++)
        for(size_t j = i+1; j < len;j++)
            if(vec.at(i) > vec.at(j))
                std::swap(vec.at(i), vec.at(j));
}
//-----------------------------------------------------------------------------

template <class X> LLONG bin_search(const std::vector<X>& vec, X elem)
{
    LLONG left = 0;
    LLONG right = vec.size()-1;
    while(left <= right){
        LLONG half = (left+right)/2;
        if(vec[half] == elem)
            return half;
        else
            vec[half] < elem ? left=half+1 : right=half-1;
    }
    return -1;
}

template <class X> LLONG lin_search(std::vector<X>& vec, X elem)
{
    size_t right = vec.size()-1;
    X last = vec.at(right);
    vec.at(right) = elem;
    size_t index = 0;
    while(vec.at(index) != elem)
        index++;
    vec.at(right) = last;
    if(index < right || vec.at(right) == elem)
        return index;
    return -1;
}
//-----------------------------------------------------------------------------

#endif // FUNC_T_H
